package com.mikearte.main;

import java.util.ArrayList;
import java.util.List;

public class UrlListContainer {
    List<String> urlList;

    UrlListContainer(){
        urlList = new ArrayList<>();
    }

    UrlListContainer(List<String> urlList){
        this.urlList = urlList;
    }

    public synchronized void add(String url){
        if (url == null || url.equals(""))
            return;

        if (!urlList.contains(url))
            urlList.add(url);
    }

    public synchronized String getFirst(){
        if (urlList.size() > 0)
            return urlList.remove(0);
        else
            return null;
    }

    public synchronized List<String> getUrlList(){
        return urlList;
    }
}

package com.mikearte.main;

import java.sql.Timestamp;
import java.util.Objects;

public class GatherProxyBean {
    public String ip;
    public Integer port;
    public String code2;
    public String country;
    public String anonymity;
    public Integer responseTime; // ms
    public String lastUpdate;
    public Timestamp lastParse;

    //LAST UPDATE	IP ADDRESS	PORT	ANONYMITY LEVE     	COUNTRY    	CITY	    UPTIME (L/D)	   RESPONSE TIMES

    GatherProxyBean(){
    }

    GatherProxyBean(String ip, Integer port, String code2, String country, String anonymity, Integer responseTime, String lastUpdate, Timestamp lastParse){
        this.ip = ip;
        this.port = port;
        this.code2 = code2;
        this.country = country;
        this.anonymity = anonymity;
        this.responseTime = responseTime;
        this.lastUpdate = lastUpdate;
        this.lastParse = lastParse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GatherProxyBean that = (GatherProxyBean) o;
        return Objects.equals(ip, that.ip) &&
                Objects.equals(port, that.port);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, port);
    }

    @Override
    public String toString() {
        return "GatherProxyBean{" +
                "ip='" + ip + '\'' +
                ", port=" + port +
                ", country='" + country + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                '}';
    }
}

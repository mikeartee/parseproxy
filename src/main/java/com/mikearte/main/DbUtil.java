package com.mikearte.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Logger;

public class DbUtil {
    private static Logger log = Logger.getLogger(ParseOneGather.class.getSimpleName());

    private static Integer saved = 0;

    private String url;
    private String user;
    private String password;

    public DbUtil(){
        ResourceBundle props = ResourceBundle.getBundle("db");


/*        Properties props = new Properties();

        try (FileInputStream in = new FileInputStream("src/main/resources/db.properties")) {
            props.load(in);
        } catch (IOException ex) {
            log.warning(ex.getMessage());
        }*/

        url = props.getString("dbUrl");
        user = props.getString("dbUser");
        password = props.getString("dbPassword");
    }

    public Integer saveGatherProxy(Set<GatherProxyBean> gatherProxyBeans){
        saved = 0;
        try(Connection con = DriverManager.getConnection(url, user, password)) {
            PreparedStatement preparedStatement;

            for (GatherProxyBean gpb : gatherProxyBeans) {
                try {
                    preparedStatement = con.prepareStatement("INSERT INTO commonProxies (id, host, port, country, anonymity, responseTime, lastParse) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)");
                    preparedStatement.setString(1, null);
                    preparedStatement.setString(2, gpb.ip);
                    preparedStatement.setInt(3, gpb.port);
                    preparedStatement.setString(4, gpb.country);
                    preparedStatement.setString(5, gpb.anonymity);
                    preparedStatement.setInt(6, gpb.responseTime);
                    preparedStatement.setTimestamp(7, gpb.lastParse);
                    saved += preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                }

            }

        } catch (SQLException sqlEx) {
            log.warning(sqlEx.getMessage());
        }

        return saved;
    }

}

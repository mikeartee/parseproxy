package com.mikearte.main;

import java.util.HashSet;
import java.util.Set;

public class ProxyBeanContainer {
    private final Set<GatherProxyBean> gatherProxyBeanSet = new HashSet<>();

    public synchronized void addGatherPtoxy(GatherProxyBean proxyBean){
        if (proxyBean != null) {
            gatherProxyBeanSet.add(proxyBean);
        }
    }

    public synchronized Set<GatherProxyBean> getGatherProxyBeanSet(){
        return gatherProxyBeanSet;
    }
}

package com.mikearte.main;

public class Waiter{
    int timeMs = 1000;

    public Waiter(){

    }

    public synchronized void Wait(Integer timeMs) throws InterruptedException {
        if (timeMs != null && timeMs > 0)
            this.timeMs = timeMs;

        wait(this.timeMs);
    }
}

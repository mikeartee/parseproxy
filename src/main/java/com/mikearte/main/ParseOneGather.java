package com.mikearte.main;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.logging.Logger;

public class ParseOneGather implements Runnable {

    private static Logger log = Logger.getLogger(ParseOneGather.class.getSimpleName());

    OkHttpClient httpClient;
    String url;
    ProxyBeanContainer proxyBeanContainer;
    UrlListContainer urlListContainer;

    public ParseOneGather(String url, UrlListContainer urlListContainer, OkHttpClient httpClient, ProxyBeanContainer proxyBeanContainer){
        this.url = url;
        this.urlListContainer = urlListContainer;
        this.httpClient = httpClient;
        this.proxyBeanContainer = proxyBeanContainer;
    }

    @Override
    public void run() {
        Request request = new Request.Builder().url(url).build();
        try(Response response = httpClient.newCall(request).execute()){
            String pageBody = response.body().string();

            Document doc = Jsoup.parse(pageBody);
            Elements lines = doc.select("div.proxy-list script");
            if (lines.size() == 0){
                urlListContainer.add(url);
                log.warning("Invalid response, reparse. "+url);
            }

            for (Element line : lines){
                GatherProxyBean proxyBean = null;

                String f = line.toString();
                String ff = f.split("\\(")[1].split("\\)")[0];

                JSONParser parser = new JSONParser();
                try {
                    JSONObject jo = (JSONObject)parser.parse(ff);
                    proxyBean = new GatherProxyBean();
                    String ip = (String) jo.get("PROXY_IP");
                    if (isIpValid(ip))
                        proxyBean.ip = (String) jo.get("PROXY_IP");
                    else
                        continue;
                    proxyBean.country = (String) jo.get("PROXY_COUNTRY");
                    proxyBean.port = Integer.parseInt((String) jo.get("PROXY_PORT"), 16);
                    proxyBean.responseTime = Integer.parseInt((String) jo.get("PROXY_TIME"));
                    proxyBean.anonymity = (String) jo.get("PROXY_TYPE");
                    proxyBean.lastParse = new Timestamp(System.currentTimeMillis());

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                proxyBeanContainer.addGatherPtoxy(proxyBean);
            }

        }catch (IOException ex){
            log.warning("Request IOException: "+ex.getStackTrace().toString());
        }
    }


    private boolean isIpValid(String ip){
        if (ip==null || ip.equals(""))
            return false;

        String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

        return ip.matches(PATTERN);
    }
}

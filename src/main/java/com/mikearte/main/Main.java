package com.mikearte.main;

import com.mikearte.okHttp.HttpInvoker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    private final static Logger log = Logger.getLogger(Main.class.getSimpleName());

    private final String baseUrl1 = "http://hideme.ru/proxy-list/";
    private final static String baseUrl2 = "https://free-proxy-list.net/";
    private final static String baseUrl3 = "http://www.gatherproxy.com";


    public static void main(String[] args) {
        long time1 = System.currentTimeMillis();


        File folderLogs = new File("logs");
        if (!folderLogs.exists())
            folderLogs.mkdir();


        try {
            LogManager.getLogManager().readConfiguration(
                    Main.class.getResourceAsStream("/logging.properties"));
            log.info("Parse has begun");
        } catch (IOException e) {
            log.info("Could not setup logger configuration: "+e.getMessage());
        }

        System.out.println("Parse has begun");
        DbUtil dbUtil = new DbUtil();
        HttpInvoker invoker = new HttpInvoker();

        List<String> countryUrlList = invoker.parseCountryUrlList(baseUrl3 + "/proxylistbycountry", "ul.pc-list > li > a");
        List<String> urlList = new ArrayList<>();
        for (String countryPath : countryUrlList) {
            urlList.add(baseUrl3 + countryPath);
        }
        UrlListContainer urlListContainer = new UrlListContainer();

        if (countryUrlList.isEmpty()) {
            log.log(Level.WARNING, "Не нашел списка адресов со странами.");
            return;
        }

        log.info("Всего ссылок на страницы разных стран: " + countryUrlList.size());

        ProxyBeanContainer beanContainer = new ProxyBeanContainer();
        ExecutorService executorService = Executors.newFixedThreadPool(10);


        for (String link : countryUrlList) {
            if (link == null || link.equals(""))
                continue;

            Runnable parseOneGather = new ParseOneGather(baseUrl3 + link, urlListContainer, invoker.getHttpClient(), beanContainer);
            executorService.execute(parseOneGather);
        }

        Waiter waiter = new Waiter();


        long time3 = System.currentTimeMillis();
        int count = 0;
        while (!executorService.isTerminated()) {
            try {
                int i = 500;
                waiter.Wait(i);
                log.info("Страниц для повтороного парса: " + urlListContainer.getUrlList().size());

                if (!executorService.isShutdown() && urlListContainer.getUrlList().isEmpty() && count == 2) {
                    executorService.shutdown();
                }
                count++;

                while (!executorService.isShutdown() && !urlListContainer.getUrlList().isEmpty()) {
                    count = 0;

                    String url = urlListContainer.getFirst();
                    if (url == null)
                        break;

                    Runnable parseOneGather = new ParseOneGather(url, urlListContainer, invoker.getHttpClient(), beanContainer);
                    executorService.execute(parseOneGather);
                }


            } catch (InterruptedException ex) {
                log.warning(ex.getStackTrace().toString());
            }
        }


        long time4 = System.currentTimeMillis();
        Set<GatherProxyBean> gatherProxyBeanSet = beanContainer.getGatherProxyBeanSet();

        log.info("Парс завершен");
        log.info("Время исполнения потоков с ожиданием: " +(time4 - time3)+ "мс");
        log.info("Общее время исполнения: " +(time4 - time1)+ "мс");
        log.info("Собрано адресов: "+ gatherProxyBeanSet.size());


        Integer saved = dbUtil.saveGatherProxy(gatherProxyBeanSet);

        log.info("Сохраненно в базу: " + saved);

        long time5 = System.currentTimeMillis();
        System.out.println("Time spend: " +(time5-time1)+ " ms.");
        System.out.println("Saved proxy to DB: " + saved);
        System.out.println("Parse ended");
    }
}

package com.mikearte.okHttp;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpInvoker {
   public final OkHttpClient httpClient = new OkHttpClient();

    public HttpInvoker(){

    }

    private String run (String url) throws IOException {
        if (url==null)
            return null;
        OkHttpClient clonedClient = httpClient;
        Request request = new Request.Builder().url(url).build();
        try(Response response = httpClient.newCall(request).execute()){
            return response.body().string();
        }
    }

    public OkHttpClient getHttpClient() {
        return httpClient.newBuilder().build();
    }

    public List<String> parseCountryUrlList(String url, String cssQuery){
        List<String> list = new ArrayList<>();

        OkHttpClient client = getHttpClient();
        Request request = new Request.Builder().url(url).build();

        try(Response response = client.newCall(request).execute()){
            String pageBody = response.body().string();
            Document doc = Jsoup.parse(pageBody);
            Elements lines = doc.select(cssQuery);

            for (Element el : lines){
                String link = el.attr("href");
                if (!link.isEmpty())
                    list.add(link);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return list;
    }
}
